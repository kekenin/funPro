package tehtavat302;

import java.util.function.DoubleUnaryOperator;

public class Makihyppy {

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
            return (x) -> {return x*lisapisteet+kPiste;};
    }
        
    public static void main(String[] args) {

       
       DoubleUnaryOperator normaaliLahti = makePistelaskuri(60, 2.0);
       
       System.out.println(normaaliLahti.applyAsDouble(100)); 
          
    }
    
}