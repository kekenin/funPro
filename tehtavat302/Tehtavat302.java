/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat302;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import static tehtavat302.Makihyppy.makePistelaskuri;

/**
 *
 * @author Beni
 */
public class Tehtavat302 {

    //TEHTAVA 1 KÄYTETTY RAJAPINTA
    public static int generaattori(IntSupplier supplier) {
        return supplier.getAsInt();

    }

    //TEHTAVA 2 OPERAATIO
    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet) {
        return (x) -> {
            return x * lisapisteet + kPiste;
        };
    }

    public static void main(String[] args) {
        System.out.println("\n************************TEHTAVA 1***********************************\n");

        //TEHTAVA 1
        System.out.println(generaattori(() -> 5));
        System.out.println("\n************************TEHTAVA 2***********************************\n");

        //TEHTAVA 2
        DoubleUnaryOperator normaaliLahti = makePistelaskuri(60, 2.0);
        System.out.println(normaaliLahti.applyAsDouble(100));

        System.out.println("\n************************TEHTAVA 3***********************************\n");

        System.out.println("*********************1/2*************************");

        //TEHTAVA 3 1/2
        Random r = new Random();
        IntStream numbers = r.ints(1, (40 + 1)).limit(7);
        numbers.forEach(System.out::println);

        System.out.println("*********************2/2*************************");
        
        //TEHTAVA 3 2/2
        IntStream.generate(new IntSupplier() {
            @Override
            public int getAsInt() {
                return r.nextInt((40 - 1) + 1) + 1;
            }
        }).limit(7).forEach(System.out::println);

        
        System.out.println("\n************************TEHTAVA 4***********************************\n");

        
        
        
        
        System.out.println("\n************************TEHTAVA 5***********************************\n");

        
        
                   
       Function siirto = Piste.makeSiirto(1, 2);
       Function skaalaus = Piste.makeSkaalaus(2);
       Function kierto = Piste.makeKierto(Math.PI);
       Function muunnos = siirto.andThen(skaalaus).andThen(kierto);
       
       Piste[] pisteet = {new Piste(1,1), new Piste(2,2), new Piste(3,3)};
       
       
       List<Piste> uudetPisteet = new CopyOnWriteArrayList();
       
       for (Piste p: pisteet){
           uudetPisteet.add((Piste)muunnos.apply(p));
       } 
  
       uudetPisteet.forEach(p -> System.out.println(p));
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }

}
