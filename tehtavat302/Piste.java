/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat302;

import java.util.function.Function;

/**
 *
 * @author Beni
 */
class Piste {

    int x, y;

    public Piste(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public Piste() {
        
    }

    static Function <Piste, Piste> makeSkaalaus(int i) {
        System.out.println("makeSkaalaus-funktio\n");
        return (Piste p) -> new Piste(p.x*i, p.y*i);
    }

    static Function<Piste, Piste> makeKierto(double PI) {
        System.out.println("makeKierto-funktio\n");
        return (Piste p) -> new Piste(p.x*1, p.y*1);
    }

    static Function<Piste,Piste> makeSiirto(int x, int y) {
        System.out.println("makeKierto-funktio\n");
        return (Piste p) -> new Piste(p.x*x, p.y*y);
    }

}
