/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat402;

import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collector;

/**
 *
 * @author Beni
 */
public class Tehtavat402 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //TEHTÄVÄ 4
        //List<Omena> omenaLista = omenaStream.collect(
        //    ArrrayList::new,
        //    List::add,
        //    List::addAll);
        //Omena-luokan ilmentymistä koostuvasta listasta tehdään streami,
        //jonka jälkeen streamin alkiot redusoidaan luotuun arraylistaan
        //collect-metodirunkoa käyttämällä.
        
        System.out.println("****************************TEHTÄVÄ 6*************************************");
        //TEHTAVA 6
        UnaryOperator<Dokumentti> valitPois
                = (Dokumentti d)
                -> { System.out.println("valitPois");
            d = new Dokumentti(d.teksti);
            d.teksti= d.teksti.replaceAll(" +", " ");
            
            return d;
        };
        
        UnaryOperator<Dokumentti> skanditPois
                = (Dokumentti d)
                -> { System.out.println("skanditPois");
            d = new Dokumentti(d.teksti);
            d.teksti= d.teksti.trim().replaceAll("ä", "a").replaceAll("å", "a").replaceAll("ö", "o")
                    .replaceAll("Å","A").replaceAll("Ä", "A").replaceAll("Ö", "O");

            return d;
        };
        
        UnaryOperator<Dokumentti> vaaratPois
                = (Dokumentti d)
                -> { System.out.println("väärätPois");
            d = new Dokumentti(d.teksti);
            d.teksti= d.teksti.trim().replaceAll("sturct", "");

            return d;
        };

        Function<Dokumentti, Dokumentti> ketju = valitPois.andThen(skanditPois).andThen(vaaratPois);
        String text = "Tässä    Ön paljon jotåin tekstiä ja vääriä sanoja kuten: sturct";
        Dokumentti d = new Dokumentti(text);

        d = ketju.apply(d);

        System.out.println(d.teksti);

    }

}
