(ns tehtavat602.core
  (:gen-class))

(defn -main []
  (println "Hello, World!"))





;;TEHTÄVÄ 1
(defn allekirjoitus [x y]
  (print x y))


(def asiallinen (partial allekirjoitus "Ystävällisin terveisin,\n"))
(def kaverille (partial allekirjoitus "T:"))



(asiallinen "pekka")
(kaverille "pekka")







;;TEHTÄVÄ 2
(def vektori [[1 2 3][4 5 6][7 8 9]])

;;A)
(def vektori-ordered (map #(apply min %)vektori))

(print vektori-ordered)

;;B)
(apply vector vektori-ordered)





;;TEHTÄVÄ 3
(def vampire-database
     
  {0 {:makes-blood-puns? false, :has-pulse? true  :name "McFishwich"}   
   
   1 {:makes-blood-puns? false, :has-pulse? true  :name "McMackson"}
   
   2 {:makes-blood-puns? true,  :has-pulse? false :name "Damon Salvatore"}
   
   3 {:makes-blood-puns? true,  :has-pulse? true  :name "Mickey Mouse"}})
      

(defn lisaa-vampyyrikantaan [db mbp hp nimi]
 (merge-with
  list
  vampire-database
  {(count vampire-database){:makes-blood-puns? mbp, :has-pulse? hp, :name nimi}}))

(lisaa-vampyyrikantaan vampire-database true true "Testi-vampire")





;;TEHTÄVÄ 4
(defn poista-vampyyri[n]
  (dissoc vampire-database n))
  
(poista-vampyyri 3)





;;TEHTÄVÄ 5

(def aines{:aines "Vesi", :yksikko "litraa", :maara 4})

{:aines "Sokeri", :yksikko "grammaa", :maara 500} 
{:aines "Sitruuna", :yksikko "kpl", :maara 2} 
{:aines "Hiiva", :yksikko "grammaa", :maara 1}

(defn monikerta[aines, n]
 update aines :maara (*(aines :maara)n))

(monikerta aines, 2)
(monikerta aines, 4)



