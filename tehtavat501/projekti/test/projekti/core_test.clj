;;(ns projekti.core-test
;;  (:require [clojure.test :refer :all]
;;            [projekti.core :refer :all]))


(ns projekti.core-test
	(:use projekti.core)
	(:use midje.sweet))


(facts "square tehtävä 4"
  (square 2) => 4
  (square 7) => 49
  (square -3) => 9)

