(ns tehtavat502.core
  (:gen-class))

(defn -main []
  (println "Hello, World!"))


;TEHTÄVÄ 1&2

(defn parillinen []
  (println "Anna luku:")
  (loop [input (read-line)]
    (if (> (Integer/parseInt input) 0)
      (do 
        (if (even? (Integer/parseInt input))
          (println "luku on parillinen")
          (print "luku on pariton")))
        
      (do
        (println "Anna isompi luku kuin 0")
        (recur(parillinen))))))

(parillinen)

;TEHTÄVÄ 3

(defn jaolliset [x]
  (def y (Integer/parseInt x))
  (loop [x y]  
    (when (> x 1)
      (if(integer?(/ x 3))
        (do
          (println x)
          (recur (- x 1)))
        (recur (- x 1))))))
      

    

(jaolliset"20")
