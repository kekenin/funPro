(ns t601.core
  (:gen-class))

(defn -main []
  (println "Hello, World!"))



;;TEHTÄVÄ 1

;LASKEE KESKIARVOT TAULUKOISTA UUTEEN TAULUKKOON
(defn average [coll] 
  (/ (reduce + coll) (count coll)))

(defn transpose [coll]
   (apply map vector coll))





;LÄMPÖTILAT TAULUKOISSA
(def data [[-10 -1 2 3 5 10 15 18 18 16 8 2 1] [-5 -2 1 3 10 20 21 19 16 8 3 -2]])


;LÄMPÖTILOJEN KESKIARVOT UUTEEN TAULUUN
(def keskilampotilat(map average (transpose data)))

;POSITIIVISET ARVOT UUTEEN TAULUUN
(def positiiviset(filter pos? keskilampotilat))

;POSITIIVISTEN KESKIARVO
(def keskiarvo (average positiiviset))


(print keskiarvo)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;TEHTÄVÄ 2


(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])



(def huhtikuu(filter #(> (:kk %) 3) food-journal))

(def nesteetHuhtikuu(reduce + (map :neste huhtikuu)))

(print nesteetHuhtikuu)

;(doseq [item nesteet]
;  (println item))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;TEHTÄVÄ 3



